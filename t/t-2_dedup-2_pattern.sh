#!/bin/bash
source common.bash

# Test that thinsheep deduplicates many identical blocks to one.

( seq 1e99 || true ) | dd iflag=fullblock of=upstream/test bs=$block_size count=16 status=none
run_thinsheep
( yes || true ) | dd of=target/devs/test bs=$block_size count=16 status=none conv=notrunc
stop_thinsheep
test "$(get_usage data/sheepdata)" -le $((2 * block_size))

# Delete pattern (test unreferencing and unhashing)
run_thinsheep
dd if=/dev/zero of=target/devs/test bs=$block_size count=16 status=none conv=notrunc
stop_thinsheep
test "$(get_usage data/sheepdata)" -le $((3 * block_size))
