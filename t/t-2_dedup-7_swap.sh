#!/bin/bash
source common.bash

# Test swapping data on two upstream devices.

( seq 1 1e99 || true ) | dd iflag=fullblock of=upstream/a bs=$block_size count=16 status=none
( seq 2 1e99 || true ) | dd iflag=fullblock of=upstream/b bs=$block_size count=16 status=none
run_thinsheep
dd if=target/devs/a of=/dev/null bs=$block_size status=none
dd if=target/devs/b of=/dev/null bs=$block_size status=none
dd if=upstream/a of=target/devs/b bs=$block_size status=none conv=notrunc
dd if=upstream/b of=target/devs/a bs=$block_size status=none conv=notrunc
stop_thinsheep

run_thinsheep
diff -q upstream/a target/devs/b
diff -q upstream/b target/devs/a
stop_thinsheep
diff -u <(get_usage data/sheepdata) /dev/stdin <<< 0
