#!/bin/bash
source common.bash

# Test that upstream devices are readable.

( seq 1e99 || true ) | dd iflag=fullblock of=upstream/test bs=$block_size count=16 status=none
run_thinsheep
diff -q target/devs/test upstream/test
stop_thinsheep
