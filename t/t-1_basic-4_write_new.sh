#!/bin/bash
source common.bash

# Test that thinsheep can preserve data written to it, even across restarts.

( seq     1e99 || true ) | dd iflag=fullblock of=upstream/test bs=$block_size count=16 status=none
( seq 1e9 1e99 || true ) | dd iflag=fullblock of=newdata       bs=$block_size count=16 status=none
run_thinsheep
dd if=newdata of=target/devs/test bs=$block_size count=16 status=none conv=notrunc
stop_thinsheep
run_thinsheep
diff -q target/devs/test newdata
stop_thinsheep
