#!/bin/bash
source common.bash

# Test that writing the same data to thinsheep does not corrupt it.

( seq 1e99 || true ) | dd iflag=fullblock of=upstream/test bs=$block_size count=16 status=none
run_thinsheep
dd if=upstream/test of=target/devs/test bs=$block_size count=16 status=none conv=notrunc
stop_thinsheep
run_thinsheep
diff -q target/devs/test upstream/test
stop_thinsheep
